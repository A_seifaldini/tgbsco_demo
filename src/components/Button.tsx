import React, { ReactNode } from "react";
import { makeStyles, Button } from "@material-ui/core";

interface ButtonProps {
  children: ReactNode;
  variant: "submit" | "exit";
  style?: React.CSSProperties | undefined;
  onClick: () => void;
}

const useStyles = makeStyles(() => ({
  submitBtn: {
    fontFamily: "Vazir",
    fontSize: 13,
    backgroundColor: "#2980b9",
    color: "white",
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 10,
    "&:hover": {
      backgroundColor: "#206999",
    },
  },
  exitBtn: {
    fontFamily: "Vazir",
    fontSize: 13,
    backgroundColor: "#c0392b",
    color: "white",
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 10,
    "&:hover": {
      backgroundColor: "#9e3024",
    },
  },
}));

export default ({ children, variant, onClick, style }: ButtonProps) => {
  const classes = useStyles();
  return (
    <Button
      className={variant === "submit" ? classes.submitBtn : classes.exitBtn}
      {...{ onClick, style }}
    >
      {children}
    </Button>
  );
};
