import React from "react";
import { makeStyles, Card, Typography } from "@material-ui/core";

interface RenderCommitsProps {
  avatar: string;
  username: string;
  message: string;
}

const useStyles = makeStyles(() => ({
  card: {
    width: "100%",
    marginBottom: 25,
    padding: 5,
    display: "flex",
    flexDirection: "row",
    direction: "ltr",
  },
  image: {
    width: "15%",
    height: "15%",
    marginRight: 15,
    borderRadius: 1000,
  },
  username: {
    fontSize: 20,
    fontWeight: "bold",
  },
  message: {
    fontSize: 14,
  },
}));

export default ({ avatar, username, message }: RenderCommitsProps) => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <img src={avatar} alt={username} className={classes.image} />
      <div>
        <Typography className={classes.username}>{username}</Typography>
        <Typography className={classes.message}>{message}</Typography>
      </div>
    </Card>
  );
};
