import React, { Fragment } from "react";
import { makeStyles, Typography, TextField } from "@material-ui/core";

import Button from "./Button";

interface InputProps {
  label: string;
  placeholder: string;
  value: string;
  btnLabel: string;
  exit?: boolean;
  password?: boolean;
  onChange: (e: any) => void;
  onClick: () => void;
  exitOnClick?: () => void;
}

const useStyles = makeStyles(() => ({
  inputContainer: {
    display: "flex",
    flex: 1,
    alignItems: "center",
  },
  label: {
    fontFamily: "Vazir",
    color: "black",
    marginBottom: 5,
    minWidth: 100,
  },
  placeholder: {
    color: "black",
    fontSize: 14,
  },
  btnContainer: {
    display: "flex",
    flexDirection: "row",
    flex: 0.3,
    justifyContent: "space-between",
    alignItems: "center",
  },
}));

const Input = ({
  label,
  placeholder,
  exit,
  password,
  btnLabel,
  onChange,
  onClick,
  exitOnClick,
}: InputProps) => {
  const classes = useStyles();
  return (
    <Fragment>
      <div className={classes.inputContainer}>
        <Typography align="right" className={classes.label}>
          {label} :
        </Typography>
        <TextField
          variant="outlined"
          size="small"
          placeholder={placeholder}
          type={password ? "password" : "text"}
          inputProps={{
            className: classes.placeholder,
            style: { direction: "ltr" },
          }}
          {...{ onChange }}
        />
      </div>
      <div className={classes.btnContainer}>
        <Button variant="submit" {...{ onClick }}>
          {btnLabel}
        </Button>
        {exit && (
          <Button variant="exit" onClick={() => exitOnClick}>
            خروج
          </Button>
        )}
      </div>
    </Fragment>
  );
};

Input.defaultProps = {
  exit: false,
  password: false,
};

export default Input;
