import React, { useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { useStyles } from "./index";

import { Input } from "../components";

export default () => {
  const classes = useStyles();
  const [repo, setRepo] = useState("");
  const history = useHistory();

  const onChange = (e: any) => {
    setRepo(e.target.value);
  };

  const onClick = () => {
    axios
      .get(`https://api.github.com/repos/${repo}`)
      .then((res) => {
        history.replace("commits", {
          commitsUrl: res.data.commits_url,
          repo: `https://api.github.com/repos/${repo}/commits`,
        });
      })
      .catch((err) => {
        console.log(err);
        alert("خطایی رخ داده لطفاً دوباره تلاش نمایید");
      });
  };

  const exitOnClick = () => {
    history.replace("/");
  };

  return (
    <Grid container className={classes.container}>
      <Grid item xs={1} md={2} lg={3} />
      <Grid item xs={10} md={8} lg={6}>
        <Paper className={classes.paperContainer}>
          <Input
            label="ریپازیتوری"
            placeholder="Facebook/react"
            value={repo}
            btnLabel="جستجو"
            exit
            {...{ onChange, onClick, exitOnClick }}
          />
        </Paper>
      </Grid>
      <Grid item xs={1} md={2} lg={3} />
    </Grid>
  );
};
