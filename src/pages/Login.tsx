import React, { useState } from "react";
import {
  Grid,
  Paper,
  Typography,
  TextField,
  makeStyles,
  Button,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";

import { useStyles } from "./index";

import { Input } from "../components";

export default () => {
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const history = useHistory();

  const onChange = (e: any) => {
    setUsername(e.target.value);
  };

  const onClick = () => {
    history.replace("/password", { username });
  };

  return (
    <Grid container className={classes.container}>
      <Grid item xs={1} md={2} lg={3} />
      <Grid item xs={10} md={8} lg={6}>
        <Paper className={classes.paperContainer}>
          <Input
            label="نام کاربری"
            placeholder="as9978"
            btnLabel="ورود"
            value={username}
            {...{ onChange, onClick }}
          />
        </Paper>
      </Grid>
      <Grid item xs={1} md={2} lg={3} />
    </Grid>
  );
};
