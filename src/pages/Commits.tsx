import React, { useEffect, useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import axios from "axios";
import { useHistory } from "react-router-dom";

import { useStyles } from "./index";

import { RenderCommits, Button } from "../components";

export default (props: {
  location: { state: { commitsUrl: string; repo: string } };
}) => {
  const { repo, commitsUrl: url } = props.location.state;

  const classes = useStyles();
  const [commits, setCommits] = useState([]);
  const history = useHistory();

  useEffect(() => {
    axios
      .get(repo)
      .then((res) => {
        setCommits(res.data);
      })
      .catch((err) => {
        console.log(err);
        alert("خطایی رخ داده لطفاً دوباره تلاش نمایید");
      });
  }, []);

  const exitOnClick = () => {
    history.replace("/");
  };

  return (
    <Grid container className={classes.container}>
      <Grid item xs={1} md={2} lg={3} />
      <Grid item xs={10} md={8} lg={6}>
        <Paper
          className={classes.paperContainer}
          style={{ flexDirection: "column", alignItems: "flex-end" }}
        >
          {commits.map((element: any, index) => {
            const avatar = element.committer.avatar_url;
            const username = element.commit.author.name;
            const message = element.commit.message;

            return (
              <RenderCommits key={index} {...{ avatar, username, message }} />
            );
          })}
          <Button
            variant="exit"
            onClick={exitOnClick}
            style={{ alignSelf: "center" }}
          >
            خروج
          </Button>
        </Paper>
      </Grid>
      <Grid item xs={1} md={2} lg={3} />
    </Grid>
  );
};
