import React, { useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { useStyles } from "./index";

import { Input } from "../components";

export default (props: { location: { state: { username: string } } }) => {
  const username = props.location.state.username;

  const classes = useStyles();
  const [password, setPassword] = useState("");
  const history = useHistory();

  const onChange = (e: any) => {
    setPassword(e.target.value);
  };

  const onClick = () => {
    const auth = btoa(username + ":" + password);
    
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Basic ${auth}`,
      },
    };

    axios
      .post("https://api.github.com/user", config)
      .then((res) => {
        if (res.data.login === username) {
          history.replace("/repo");
        }
      })
      .catch((err) => {
        console.log(err);
        alert("خطایی رخ داده لطفاً دوباره تلاش نمایید");
      });

    history.replace("/repo");
  };

  const exitOnClick = () => {
    history.replace("/");
  };

  return (
    <Grid container className={classes.container}>
      <Grid item xs={1} md={2} lg={3} />
      <Grid item xs={10} md={8} lg={6}>
        <Paper className={classes.paperContainer}>
          <Input
            label="رمزعبور"
            placeholder="********"
            value={password}
            btnLabel="ورود"
            exit
            password
            {...{ onChange, onClick, exitOnClick }}
          />
        </Paper>
      </Grid>
      <Grid item xs={1} md={2} lg={3} />
    </Grid>
  );
};
