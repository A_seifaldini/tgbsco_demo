import { makeStyles } from "@material-ui/core";

export { default as Login } from "./Login";
export { default as Password } from "./Password";
export { default as Repo } from "./Repo";
export { default as Commits } from "./Commits";

export const useStyles = makeStyles(() => ({
  container: {
    marginTop: 50,
    direction: "rtl",
  },
  paperContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 25,
  },
}));
