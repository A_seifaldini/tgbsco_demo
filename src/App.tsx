import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Login, Password, Repo, Commits } from "./pages";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/password" component={Password} />
        <Route exact path="/repo" component={Repo} />
        <Route exact path="/commits" component={Commits} />
      </Switch>
    </Router>
  );
}

export default App;
